import * as mocha from 'mocha';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

import app from '../src/App';

chai.use(chaiHttp);
const expect = chai.expect;
const should = chai.should();

describe('GET api/v1/route', function () {

  it('returns 400 if no url parameters "from" and "to"', function (done) {
    chai.request(app.app).get('/api/v1/route')
    .end(function (err, res) {
      res.should.have.status(400);
      res.should.be.json;
      res.body.should.have.property('message');
      res.body.message.should.equal(`Query parameters 'from' and 'to' are required and must include iata/icao (3/4 chars) code for the airport!`);
      done();
    });
  });

  it('returns 400 if "from" and "to" url param values not 3 or 4 chars long', function (done) {
    chai.request(app.app).get('/api/v1/route')
    .query({from: 'tll', to:'prggg'})
    .end(function (err, res) {
      res.should.have.status(400);
      res.should.be.json;
      res.body.should.have.property('message');
      res.body.message.should.equal(`Query parameters 'from' and 'to' are required and must include iata/icao (3/4 chars) code for the airport!`);
      done();
    });
  });

  it('returns 404 if not found', function (done) {
    chai.request(app.app).get('/api/v1/route')
    .query({from: 'tll', to:'ffs'})
    .end(function (err, res) {
      res.should.have.status(404);
      res.should.be.json;
      res.body.message.should.equal(`Route not found`);
      res.body.from.should.equal(`TLL`);
      res.body.to.should.equal(`FFS`);
      done();
    });
  });

  it('returns 200 if found', function (done) {
    chai.request(app.app).get('/api/v1/route')
    .query({from: 'tll', to:'prg'})
    .end(function (err, res) {
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.have.property('airports');
      res.body.should.have.property('hops');
      res.body.should.have.property('hop_count');
      res.body.should.have.property('total_dist_km');
      done();
    });
  });
});