import { Router, Request, Response, NextFunction } from 'express';
import app from '../App'

class FlightRouter {
  router: Router
  constructor() {
    this.router = Router();
    this.router.get('/', this.getRoute);
  }

  public getRoute(req: Request, res: Response, next: NextFunction) {
    let error400 = {
      status: 400,
      status_text: 'Bad Request',
      message: `Query parameters 'from' and 'to' are required and must include iata/icao (3/4 chars) code for the airport!`
    }
    let from = null;
    let to = null;
    let from_type = null;
    let to_type = null;

    if (!req.query.hasOwnProperty('from') || !req.query.hasOwnProperty('to')) {
      return res.status(400).json(error400);
    }

    if (req.query.from.length == 3 && req.query.to.length == 3) {
      from_type = 'iata';
      to_type = 'iata';
    }
    if (req.query.from.length == 4 && req.query.to.length == 4) {
      from_type = 'icao';
      to_type = 'icao';
    }
    if (req.query.from.length == 3 && req.query.to.length == 4) {
      from_type = 'iata';
      to_type = 'icao';
    }
    if (req.query.from.length == 4 && req.query.to.length == 3) {
      from_type = 'icao';
      to_type = 'iata';
    }

    if (from_type == null || to_type == null) {
      return res.status(400).json(error400);
    }

    let change = ''
    if (req.query.changes == 'true') {
      change = 'CHANGE|';
    }

    let cypher = `MATCH (from:Airport {${from_type}:{from}}), (to:Airport {${to_type}:{to}})
    CALL apoc.algo.dijkstra(from, to, '${change}FLIGHT_ROUTE>','dist')
    YIELD path, weight
    RETURN
    nodes(path) as airports,
    relationships(path) as hops,
    length(path) as hop_count,
    weight as total_dist_km`;

    from = req.query.from.toUpperCase(),
    to = req.query.to.toUpperCase(),

    app.db.query(cypher, {from: from, to: to}, function(err, result) {
      if (err) {
        return res.status(500).json({ status: 500, status_text: 'Internal Server Error' });
      }
      if (result.length == 0) {
        return res.status(404).json({
          status: 404,
          status_text: 'Not Found',
          from: from,
          to: to,
          message: `Route not found`
        });
      }
      res.json(result[0]);
    });

  }
}

// Create the Router, and export its configured Express.Router
const flightRoutes = new FlightRouter();
export default flightRoutes.router;