import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import FlightRouter from './routes/FlightRouter';

// Creates and configures an ExpressJS web server.
class App {

  // ref to Express instance
  public app: express.Application;
  public db;

  //Run configuration methods on the Express instance.
  constructor() {
    this.app = express();
    this.db = require("seraph")("http://neo4j:7474");
    //this.db = require("seraph")("http://localhost:17474");
    this.middleware();
    this.routes();
  }

  // Configure Express middleware.
  private middleware(): void {
    this.app.use(logger('dev'));
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(this.cleanup);
  }

  // Configure API endpoints.
  private routes(): void {
    this.app.use('/api/v1/route', FlightRouter);
  }

  private cleanup(req, res, next) {
    res.on('finish', function () {
      if(req.neo4jSession) {
        req.neo4jSession.close();
        delete req.neo4jSession;
      }
    });
    next();
  };
}

const app = new App();
export default app;