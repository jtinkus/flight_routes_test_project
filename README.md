# Flight Routes Test Project

Flight Routes Test Project (FRTP) is test project to calculate shortest flight routes based on flight routes and airports data from [OpenFlights.org](https://openflights.org) and serve the results over REST endpoint as a JSON payload.

### Used technologies

* Data storage - [Neo4J graph database](https://neo4j.com/) community edition.
    * [Neo4j Spatial](https://github.com/neo4j-contrib/spatial) plugin for geographic calculations.
    * [APOC](https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases) plugin for fast graph algorithms.
* Node.js, Express, Typescript etc for webserver and api implementation.
* Mocha, Chai etc for testing.
* Seraph javascript drivers for Neo4j.
* Python + requests and py2neo for downloading flight data and initializing database.
* Docker Community edition for running the project in container environment.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

* Git - [see here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* Docker running - [see here](https://www.docker.com/community-edition#/download)
* Optionally for running database init script:
    * Python 2.7  - [see here](https://www.python.org/download/releases/2.7/)
    * pip - [see here](https://pip.pypa.io/en/stable/installing/)
* Optionally for running tests locally:
    * Node + npm installed


### Installing

Clone git repo:

```
git clone https://jtinkus@bitbucket.org/jtinkus/flight_routes_test_project.git
```

Navigate to the folder: 

<repo_root>/flight_routes_test_project/node-rest-api 

Run (note that docker might need elevated rights in some environments):
```
docker-compose up
```

This will download Node.js and Neo4j docker images, build application and start the app and database up. Hopefully :)

* Rest api will be served (when ran from docker image) at: http://localhost:13000/api/v1/route
* Database browser will be served at: http://localhost:17474/browser/

As database initialization takes some time (about 10-15 min) there is initialized data set already included in repo and linked to be available for database to consume. There are also required plugins and config besides database data in folder  

<repo_root>/flight_routes_test_project/node-rest-api/neo4j_data. 

Also databse logs will be saved into this folder.

## REST api

Endpoint accepts GET request with mandatory following url parameters:
* from - mandatory - IATA or ICAO code of the starting airport, 3 or 4 chars.
* to - mandatory - IATA or ICAO code of the starting airport, 3 or 4 chars.
* changes - optional - if value is 'true' then non-flight changes between airports closer than 100 km are also included in shortest path search.

IATA and ICAO codes can be used in same request.

If flight routes between given airports are found, shortest one (geographical distance in km-s) is returned as JSON object. In response JSON there is following data: 
* airports on the route, 
* hops on the route (flights and/or changes), 
* number of hops on the route,
* total distance of the route in km-s.

Sample query:
http://localhost:13000/api/v1/route?from=tll&to=prg&routes=true

You can look up IATA and ICAO codes here:
[List of airports](https://en.wikipedia.org/wiki/List_of_airports_by_IATA_and_ICAO_code)


## Populating database

Database initialization script is in the <repo_root>/flight_routes_test_project/python_init_db folder. 

To install required packages, run the following in the python_init_db folder:
```
pip install -r requirements.txt
```
Script expects Neo4j database available on http://localhost:17474/. This is the same address where docker image is configured to expose its endpoint. This can be changed in the script main method. 

Running the script:
```
python init_db.py
```

## Running the integration tests

Get the docker running (for the database being available), change database url in App.ts to localhost address (commented out there) and run in the <repo_root>/flight_routes_test_project/node-rest-api:
```
npm install
```
```
npm run build
```
```
npm run test
```


## Authors

* **Jako Tinkus** - jtinkus@gamil.com

## Acknowledgments

* Hat tip to anyone who's code was used :)

## Final note

This is not a production ready project. Not all things are completed, not all thing are perfectly polished, a lot of things could be improved.

Rather it is a proof of concept level test project :)

With best regards, 
Jako
