import csv
import sys
import requests
import json

from math import radians, cos, sin, asin, sqrt
from py2neo import Graph, Node, Relationship


def create_nodes(graph, label, sourcefile):
    fieldnames = ['airport_id', 'name', 'city', 'country', 'iata',
                  'icao', 'lat', 'lon', 'alt',
                  'timezone', 'dst', 'tz_timezone']

    with open(sourcefile) as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=fieldnames)
        nodes = {}
        for row in reader:
            node_properties = {p: row[p] for p in fieldnames}
            node_properties['lat'] = float(node_properties['lat'])
            node_properties['lon'] = float(node_properties['lon'])
            node = Node(label, **node_properties)
            graph.create(node)
            nodes[row['airport_id']] = node
        return nodes


known_distances = {}


def haversine(lat1, lon1, lat2, lon2):
    lat1, lon1, lat2, lon2 = map(radians, [lat1, lon1, lat2, lon2])

    dlat = lat2 - lat1
    dlon = lon2 - lon1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))

    r = 6367  # km
    return r * c


def get_distance(source_airport_node, destination_airport_node):
    s_id, d_id = (source_airport_node.properties['airport_id'], destination_airport_node.properties['airport_id'])
    distance = known_distances.get((s_id, d_id))
    if distance is None:
        lat1 = float(source_airport_node.properties['lat'])
        lon1 = float(source_airport_node.properties['lon'])
        lat2 = float(destination_airport_node.properties['lat'])
        lon2 = float(destination_airport_node.properties['lon'])
        distance = haversine(lat1, lon1, lat2, lon2)
        known_distances[(s_id, d_id)] = distance
        known_distances[(d_id, s_id)] = distance
    return distance


def create_routes(graph, sourcefile, airport_nodes):
    route_fieldnames = ['airline', 'airline_id', 'source_airport', 'source_airport_id',
                        'destination_airport', 'destination_airport_id',
                        'codeshare', 'stops', 'equipment']
    with open(sourcefile) as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=route_fieldnames)
        for row in reader:
            try:
                properties = {p: row[p] for p in route_fieldnames}
                source_airport_id = row['source_airport_id']
                destination_airport_id = row['destination_airport_id']
                if source_airport_id.isdigit() and destination_airport_id.isdigit():
                    source_airport_node = airport_nodes[source_airport_id]
                    destination_airport_node = airport_nodes[destination_airport_id]
                    properties['dist'] = get_distance(source_airport_node, destination_airport_node)
                    properties['type'] = 'FLIGHT_ROUTE'
                    graph.create(Relationship(source_airport_node, 'FLIGHT_ROUTE', destination_airport_node, **properties))
            except (KeyError):
                # skip routes between non existing airports
                continue


def download_data_files(url_list):
    for url in url_list:
        local_filename = url.split('/')[-1]
        r = requests.get(url, stream=True)
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    # f.flush()


def main():

    # Configure here
    neo4j_uri = 'http://localhost:17474/db/data/'
    airports_file_name = 'airports.dat'
    routes_file_name = 'routes.dat'
    airports_file_url = 'https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat'
    routes_file_url = 'https://raw.githubusercontent.com/jpatokal/openflights/master/data/routes.dat'

    print("Downloading data files...")
    download_data_files([airports_file_url, routes_file_url])

    # Create spatial layer
    #  all seems fine, but queries do not work if layer created by func call, using rest api instead
    # graph.data("call spatial.addPointLayerWithConfig('test', 'lat:lon') yield node return node")
    headers = {'content-type': 'application/json', 'X-Stream': 'true'}
    url = neo4j_uri + "ext/SpatialPlugin/graphdb/addSimplePointLayer"
    payload = {
        "layer": "geom",
        "lat": "lat",
        "lon": "lon"
    }
    r = requests.post(url, data=json.dumps(payload), headers=headers)

    print("Creating graph...")
    graph = Graph(neo4j_uri)

    print("Creating airport nodes...")
    airport_nodes = create_nodes(graph, 'Airport', airports_file_name)

    print("Creating routes...")
    create_routes(graph, routes_file_name, airport_nodes)

    # Remove airports with no routes connected to them
    graph.data("MATCH (a:Airport) WHERE NOT (a)-[:FLIGHT_ROUTE]-() detach delete a")

    # add nodes to spatial layer for calculating graph edges for airport changes
    print ("Adding nodes to Spatial layer...")
    r = graph.data("MATCH (a:Airport) WITH collect(a) AS airports CALL spatial.addNodes('geom', airports) YIELD count RETURN count")

    # add CHANGE relation between airports with distance less than 100 km
    print("Creating airport changes...")
    r = graph.data("MATCH (a:Airport) WITH collect(a) AS airports UNWIND airports as ap "
                   "CALL spatial.withinDistance('geom', {lat:ap.lat, lon:ap.lon}, 100.0) "
                   "yield node, distance with collect([ap, node, distance]) as changes "
                   "unwind changes as ch with ch[0] as a, ch[1] as b, ch[2] as dist merge (a)-[r:CHANGE {type:'CHANGE'}]-(b) SET r.dist = dist")

    # delete changes with same node
    r = graph.data("MATCH (a:Airport)-[r:CHANGE]-(a) delete r")

    print("Done...")
    return 0


if __name__ == '__main__':
    sys.exit(main())
